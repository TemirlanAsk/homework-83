const express = require('express');
const Album = require('../models/Album');

const  createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {

        if(req.query.artist) {
            Album.find({artist: req.query.artist})
                .then(results = res.send(results))
                .catch(() => res.sendStatus(500));
    } else {
            Album.find()
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        }
    });

    router.post('/', (req, res) => {
        const album = new Album(req.body);

        album.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });
    router.get('/:id', (req, res) => {
        const id = req.params.id;
        Album.findOne({_id: req.params.id})
            .then(result => {
                if(result) res.send(result);
                else res.sendStatus(404);
            })
            .catch(() => res.sendStatus(500));
    });
    return router;

};
module.exports = createRouter;