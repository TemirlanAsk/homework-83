const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Artist = require('../models/Artists');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

const createRouter  = (db) => {
    router.get('/', (req, res) => {
        Artist.find().populate('album')
            .then(results => res.send(results))
            .catch(() => rea.senStatus(500));
    });

    router.post('/', upload.single('image'), (req, res) => {
        const artistData = req.body;

        if(req.file){
            artistData.image = req.file.filename;
        } else {
            artistData.image = null;
        }
        const artist = new Artist(artistData);
        artist.save()
            .then(result => res.send(result))
            .catch(error => res.Status(400).send(error));
    });
        router.get('/:id', (req, res) => {
            const id = req.params.id;
            db.collection('artists')
                .findOne({_id: new ObjectId(req.params.id)})
                .then(result => {
                    if (result) res.send(result);
                    else res.sendStatus(404);
                })
                .catch(() => res.sendStatus(500));

        });

    return router;

};
module.exports = createRouter;