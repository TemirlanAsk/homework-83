const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artists');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {

    const artist = Artist.create({
        title: 'Eminem',
        decription: 'Eminem is an American rapper, record producer and actor known as one of the most controversial and best-selling artists of the early 21st century.'
    }, {
        title: 'Anthony Kiedis',
        description: 'Anthony Kiedis is the lead singer of the Red Hot Chili Peppers'
    });



    db.close();
});

