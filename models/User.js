const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const SALT_WORK_FACTOR = 10;


const UserSchema = new Schema ({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    }
});

UserSchema.pre('save', async function(next){
        const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
        const hash = await bcrypt.hash(this.password, salt);

        this.password = hash;

        next();
});
UserSchema.set('toJSON', {
        transform: (doc, ret, options) => {
            delete  ret.password;
            return ret;

        }
});

const User = mongoose.model('User', UserSchema);

module.exports =  UserSchema;


