const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');
const album = require('./app/album');
const artists = require('./app/artists');
const track = require('./app/tracks');
const trackhistory = require('./app/trackhistory');
const user = require('./app/user');

const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

// mongodb://localhost:27017/shop
mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', () => {
  console.log('Mongoose connected!');

  app.use('/albums', album());
  app.use('/artists', artists());
  app.use('/tracks', track());
  app.use('/trackshistory', trackhistory());
  app.use('/user', user());

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});
